﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ByteArray
{
    public class BReader: IDisposable
    {
        #region Properties

        private bool m_disposed = false;
        private BinaryReader m_reader;
        private List<int> LastReads = new List<int>();

        /// <summary>
        ///   Gets availiable bytes number in the buffer
        /// </summary>
        public long BytesAvailable
        {
            get { return m_reader.BaseStream.Length - m_reader.BaseStream.Position; }
        }

        public long Position
        {
            get
            {
                return m_reader.BaseStream.Position;
            }
            set
            {
                m_reader.BaseStream.Position = value;
            }
        }


        public Stream BaseStream
        {
            get
            {
                return m_reader.BaseStream;
            }
        }

        public byte[] Data
        {
            get
            {
                var pos = m_reader.BaseStream.Position;

                var data = new byte[BytesAvailable];
                m_reader.BaseStream.Read(data, 0, (int)BytesAvailable);

                m_reader.BaseStream.Position = pos;

                return data;
            }
        }

        public string DataHexString
        {
            get
            {
                char[] c = new char[this.Data.Length * 2];
                int b;
                for (int i = 0; i < this.Data.Length; i++)
                {
                    b = this.Data[i] >> 4;
                    c[i * 2] = (char)(55 + b + (((b - 10) >> 31) & -7));
                    b = this.Data[i] & 0xF;
                    c[i * 2 + 1] = (char)(55 + b + (((b - 10) >> 31) & -7));
                }
                return new string(c);
            }
        }

        public string DataHexStringSpaces
        {
            get
            {
                string hexStr = DataHexString.ToUpper();
                string hexStrReadable = "";
                for (int i = 0; i < hexStr.Length; i++)
                {
                    hexStrReadable += hexStr[i];
                    if ((i + 1) % 2 == 0) hexStrReadable += " ";
                }
                return hexStrReadable;
            }
        }

        #endregion

        #region Initialisation

        /// <summary>
        ///   Initializes a new instance of the <see cref = "BReader" /> class.
        /// </summary>
        public BReader()
        {
            m_reader = new BinaryReader(new MemoryStream(), Encoding.UTF8);
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "BReader" /> class.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        public BReader(Stream stream)
        {
            m_reader = new BinaryReader(stream, Encoding.UTF8);
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "BReader" /> class.
        /// </summary>
        /// <param name = "tab">Memory buffer.</param>
        public BReader(byte[] tab)
        {
            m_reader = new BinaryReader(new MemoryStream(tab), Encoding.UTF8);
        }

        /// <summary>
        ///   Initializes a new instance of the <see cref = "BReader" /> class.
        /// </summary>
        /// <param name = "tab">Hexadecimal string of packet</param>
        public BReader(string hexaPacket)
        {
            hexaPacket = hexaPacket.Replace(" ", "");
            if (hexaPacket.Length % 2 != 0)
                throw new ArgumentException($"The binary key cannot have an odd number of digits: {hexaPacket}");
            byte[] tab = Enumerable.Range(0, hexaPacket.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(hexaPacket.Substring(x, 2), 16)).ToArray();
            m_reader = new BinaryReader(new MemoryStream(tab), Encoding.UTF8);
        }

        #endregion

        #region Public Method

        /// <summary>
        ///   Read bytes in big endian format
        /// </summary>
        /// <param name = "count"></param>
        /// <returns></returns>
        public byte[] ReadBigEndianBytes(int count)
        {
            if (count > BytesAvailable)
                throw new Exception("Packet's size is too low");
            var bytes = new byte[count];
            int i;
            for (i = count - 1; i >= 0; i--)
                bytes[i] = (byte)BaseStream.ReadByte();
            LastReads.Add(count);
            return bytes;
        }

        /// <summary>
        /// Cancel the last read (set position to the previous value)
        /// </summary>
        /// <returns>Returns if the operation is a success or no</returns>
        public bool CancelLastRead()
        {
            if (LastReads.Count < 1)
                return false;

            int NumBytesOfLastRead = LastReads.Last();
            LastReads.RemoveAt(LastReads.Count - 1);
            m_reader.BaseStream.Position -= (int)NumBytesOfLastRead;
            return true;
        }

        /// <summary>
        ///   Read a Short from the Buffer
        /// </summary>
        /// <returns></returns>
        public short ReadShort()
        {
            return BitConverter.ToInt16(ReadBigEndianBytes(2), 0);
        }


        /// <summary>
        ///   Read a int from the Buffer
        /// </summary>
        /// <returns></returns>
        public int ReadInt()
        {
            return BitConverter.ToInt32(ReadBigEndianBytes(4), 0);
        }

        /// <summary>
        ///   Read a long from the Buffer
        /// </summary>
        /// <returns></returns>
        public Int64 ReadLong()
        {
            return BitConverter.ToInt64(ReadBigEndianBytes(8), 0);
        }

        /// <summary>
        ///   Read a Float from the Buffer
        /// </summary>
        /// <returns></returns>
        public float ReadFloat()
        {
            return BitConverter.ToSingle(ReadBigEndianBytes(4), 0);
        }

        /// <summary>
        ///   Read a UShort from the Buffer
        /// </summary>
        /// <returns></returns>
        public ushort ReadUShort()
        {
            return BitConverter.ToUInt16(ReadBigEndianBytes(2), 0);
        }



        /// <summary>
        ///   Read a int from the Buffer
        /// </summary>
        /// <returns></returns>
        public UInt32 ReadUInt()
        {
            return BitConverter.ToUInt32(ReadBigEndianBytes(4), 0);
        }

        /// <summary>
        ///   Read a long from the Buffer
        /// </summary>
        /// <returns></returns>
        public UInt64 ReadULong()
        {
            return BitConverter.ToUInt64(ReadBigEndianBytes(8), 0);
        }

        /// <summary>
        ///   Read a byte from the Buffer
        /// </summary>
        /// <returns></returns>
        public byte ReadByte()
        {
            return m_reader.ReadByte();
        }

        public sbyte ReadSByte()
        {
            return m_reader.ReadSByte();
        }

        /// <summary>
        ///   Returns N bytes from the buffer
        /// </summary>
        /// <param name = "n">Number of read bytes.</param>
        /// <returns></returns>
        public byte[] ReadBytes(int n)
        {
            return m_reader.ReadBytes(n);
        }

        /// <summary>
        /// Returns N bytes from the buffer
        /// </summary>
        /// <param name = "n">Number of read bytes.</param>
        /// <returns></returns>
        public BReader ReadBytesInNewBigEndianReader(int n)
        {
            return new BReader(m_reader.ReadBytes(n));
        }

        /// <summary>
        ///   Read a Boolean from the Buffer
        /// </summary>
        /// <returns></returns>
        public Boolean ReadBoolean()
        {
            byte boolByte = m_reader.ReadByte();
            if (boolByte == 0) return false;
            else if (boolByte == 1) return true;
            else throw new Exception("There is not boolean");
        }

        /// <summary>
        ///   Read a Char from the Buffer
        /// </summary>
        /// <returns></returns>
        public Char ReadChar()
        {
            return (char)ReadUShort();
        }

        /// <summary>
        ///   Read a Double from the Buffer
        /// </summary>
        /// <returns></returns>
        public Double ReadDouble()
        {
            return BitConverter.ToDouble(ReadBigEndianBytes(8), 0);
        }

        /// <summary>
        ///   Read a Single from the Buffer
        /// </summary>
        /// <returns></returns>
        public Single ReadSingle()
        {
            return BitConverter.ToSingle(ReadBigEndianBytes(4), 0);
        }

        /// <summary>
        ///   Read a string from the Buffer
        /// </summary>
        /// <returns></returns>
        public string ReadUTF()
        {
            ushort length = ReadUShort();

            byte[] bytes = ReadBytes(length);
            return Encoding.UTF8.GetString(bytes);
        }

        public string ReadString()
        {
            ushort length = ReadByte();

            byte[] bytes = ReadBytes(length);
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        ///   Read a string from the Buffer
        /// </summary>
        /// <returns></returns>
        public string ReadUTF7BitLength()
        {
            int length = ReadInt();

            byte[] bytes = ReadBytes(length);
            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        ///   Read a string from the Buffer
        /// </summary>
        /// <returns></returns>
        public string ReadUTFBytes(ushort len)
        {
            byte[] bytes = ReadBytes(len);

            return Encoding.UTF8.GetString(bytes);
        }

        /// <summary>
        ///   Skip bytes
        /// </summary>
        /// <param name = "n"></param>
        public void SkipBytes(int n)
        {
            int i;
            for (i = 0; i < n; i++)
            {
                m_reader.ReadByte();
            }
        }


        public void Seek(int offset, SeekOrigin seekOrigin)
        {
            m_reader.BaseStream.Seek(offset, seekOrigin);
        }

        /// <summary>
        ///   Add a bytes array to the end of the buffer
        /// </summary>
        public void Add(byte[] data, int offset, int count)
        {
            long pos = m_reader.BaseStream.Position;

            m_reader.BaseStream.Position = m_reader.BaseStream.Length;
            m_reader.BaseStream.Write(data, offset, count);
            m_reader.BaseStream.Position = pos;
        }

        #endregion

        #region Dispose

        /// <summary>
        ///   Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        protected virtual void Dispose(bool itIsSafeToAlsoFreeManagedObjects)
        {
            if (!m_disposed && m_reader != null)
            {
                m_reader.Close();
                m_reader.Dispose();
                m_disposed = true;
            }
            if (itIsSafeToAlsoFreeManagedObjects && m_reader != null)
                m_reader = null;
        }

        public void Dispose() { Dispose(true); GC.SuppressFinalize(this); return; }

        ~BReader() { Dispose(false); }

        #endregion
    }
}
