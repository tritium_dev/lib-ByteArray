﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ByteArray.CustomTypes.UNumber3Bytes
{
    /// <summary>
    ///     Représente un entier non signé codé sur 3 bytes. Utilisé par TFM pour encoder des longs length de paquet.
    /// </summary>
    public static class UNumber3Bytes
    {
        [StructLayout(LayoutKind.Explicit)]
        public struct UIntInBytes
        {
            [FieldOffset(0)] public uint IntegerValue;

            [FieldOffset(0)] public byte Byte1;
            [FieldOffset(1)] public byte Byte2;
            [FieldOffset(2)] public byte Byte3;
            [FieldOffset(3)] public byte Byte4;
        }

        /// <summary>
        ///     Write an unsigned number on 3 bytes
        /// </summary>
        /// <param name="unumber">Unsigned number (max 16777215)</param>
        public static void WriteUNumber3Bytes(this BWriter writer, uint unumber)
        {
            // Si le nombre excede 3 octets (16777215), on retourne une exception
            if (unumber > 0xFFFFFF)
                throw new ArgumentException("This number cannont be writen in 3 bytes (too big number)");

            // On code l'uint en bytes, qu'on place ensuite dans un array (en ordre descendant), puis on écrit l'array
            var numberInBytes = new UIntInBytes();
            numberInBytes.IntegerValue = unumber;

            var numberBytesArray = new byte[] { numberInBytes.Byte3, numberInBytes.Byte2, numberInBytes.Byte1 };
            writer.WriteBigEndianBytes(numberBytesArray);
        }

        /// <summary>
        ///     Read an unsigned number on 3 bytes
        /// </summary>
        public static uint ReadUNumber3Bytes(this BReader reader)
        {
            // Récupération des trois bytes du nombre (rappel : ordre descendant)
            byte[] numberBytesArray = reader.ReadBigEndianBytes(3);

            // Création d'un UIntInBytes où on va mettre les bytes récupérés, puis on renvoit le nombre sous forme d'uint
            var numberInBytes = new UIntInBytes();
            numberInBytes.Byte3 = numberBytesArray[0];
            numberInBytes.Byte2 = numberBytesArray[1];
            numberInBytes.Byte1 = numberBytesArray[2];
            uint uintNumber = numberInBytes.IntegerValue;
            return uintNumber;
        }
    }
}
