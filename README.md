# ByteArray

Ce répo contient l'implémentation de ByteArray dans plusieurs langages

## Avancement
- Python : 100%
- C# : 100%
- PHP : 0%

## Notes
- En C#, le Byte (et uniquement ce type) est par défaut **non signé**. Il n'y a donc pas de UByte, mais un SByte.
